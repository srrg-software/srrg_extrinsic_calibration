#pragma once

#include "calibration_measurement.h"
#include "calibration_prior.h"
#include "camera_calibration_solver.h"
#include <srrg_nicp_tracker/base_triggers.h>
#include <Eigen/Core>
#include <limits>
#include <deque>
#include <queue>
#include <vector>
#include <fstream>

namespace srrg_extrinsic_calibration {

  class ExtrinsicCalibratorTrigger: public srrg_nicp_tracker::Tracker::Trigger{

    struct TransformPair{
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
      TransformPair(){};
      TransformPair(const Eigen::Isometry3f& odometry, 
		    const Eigen::Isometry3f& tracker_pose){
	this->odometry= odometry;
	this->tracker_pose = tracker_pose;
      }
      Eigen::Isometry3f odometry;
      Eigen::Isometry3f tracker_pose;
    };
    typedef std::map<srrg_core::BaseCameraInfo*, TransformPair, std::less <srrg_core::BaseCameraInfo*>, Eigen::aligned_allocator< std::pair<srrg_core::BaseCameraInfo*, TransformPair> > > CameraPoseMap;


  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    ExtrinsicCalibratorTrigger(srrg_nicp_tracker::Tracker* tracker,
			       int priorory);

    inline void setOutputStream(std::ostream& os) {_os = &os;}

    inline void setTfFilePrefix(const std::string& s) { _tf_file_prefix = s;}

    virtual void action(srrg_nicp_tracker::Tracker::TriggerEvent e);  

    void saveCurrentTransforms();
		      
  protected:
    std::string _tf_file_prefix;
    CameraPoseMap _camera_poses;
    std::ostream* _os;
    float _good_ratio;
    float _min_translation;
    float _min_rotation;
    int _min_measurements;
    int _iterations;
    int _num_calib_done;
    int _count;
    std::map<srrg_core::BaseCameraInfo*, CameraCalibrationSolver*> _solvers;
  };

}
