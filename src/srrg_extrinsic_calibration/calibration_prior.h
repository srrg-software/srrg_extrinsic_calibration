#pragma once

#include "calibration_measurement.h"

namespace srrg_extrinsic_calibration {

  struct CalibrationPrior{
    CalibrationPrior(const Eigen::Isometry3f& camera_offset_mean,
		     const srrg_core::Matrix6f& camera_offset_info,
		     Eigen::Isometry3f& camera_offset,
		     Eigen::Isometry3f& inverse_camera_offset);
  
    inline srrg_core::Matrix6f omega() const {
      Eigen::Matrix3f R = _inverse_camera_offset_mean.linear();
      R.transposeInPlace();
      srrg_core::Matrix6f info;
      info.setZero();
      info.block<3,3>(0,0)=R*_camera_offset_info.block<3,3>(0,0)*R.transpose();
      info.block<3,3>(3,3)=R*_camera_offset_info.block<3,3>(3,3)*R.transpose();
      return info;
    }

    Eigen::Isometry3f* _camera_offset;
    Eigen::Isometry3f* _inverse_camera_offset;

    Eigen::Isometry3f _camera_offset_mean;
    Eigen::Isometry3f _inverse_camera_offset_mean;

    srrg_core::Matrix6f _camera_offset_info;

    inline srrg_core::Vector6f error(const srrg_core::Vector6f& delta_x = srrg_core::Vector6f::Zero()) const {
      Eigen::Isometry3f delta_X=srrg_core::v2t(delta_x);
      return srrg_core::t2v(_inverse_camera_offset_mean * (*_camera_offset) * delta_X);
    }

    inline srrg_core::Matrix6f jacobian()  const {
      float epsilon = 1e-2;
      srrg_core::Matrix6f J;
      J.setZero();
      float two_inverse_epsilon = .5/epsilon;
      for (int i = 0; i<6; i++){
	srrg_core::Vector6f increment = srrg_core::Vector6f::Zero();
	increment(i) = epsilon;
	J.col(i)=two_inverse_epsilon * (error(increment) - error(-increment));
      }
      return J;
    }

  };

  typedef std::vector<CalibrationPrior, Eigen::aligned_allocator<CalibrationPrior> > CalibrationPriorVector;

}
