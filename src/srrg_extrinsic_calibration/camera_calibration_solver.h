#pragma once

#include "calibration_measurement.h"
#include "calibration_prior.h"
#include <srrg_types/base_camera_info.h>

namespace srrg_extrinsic_calibration {

  struct CameraCalibrationSolver {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CameraCalibrationSolver(srrg_core::BaseCameraInfo *cam) ;
    void addMeasurement(const Eigen::Isometry3f& base_motion,
			const Eigen::Isometry3f& camera_motion);
    void addPrior(const Eigen::Isometry3f& prior_mean,
		  const srrg_core::Matrix6f& prior_info);
    float oneRound();

    srrg_core::BaseCameraInfo* _cam;
    CalibrationMeasurementVector _measurements;
    CalibrationPriorVector _priors;
    Eigen::Isometry3f _camera_offset;
    Eigen::Isometry3f _inverse_camera_offset;
    float _damping;

  };

}
